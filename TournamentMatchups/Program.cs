﻿/**
 * Tournament Matchups
 * Creates a simple tournament bracket from a list of seeded players.
 * Bracket respects best vs worst rule, and will give top players byes if number of players is not a potency of 2.
 * Written by Bradley Meek 
 **/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TournamentMatchups
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a name for your tounament.");

            String tName = Console.ReadLine();

            Tournament t = new Tournament(tName);

            t.runTournament();

            Console.ReadLine();
        }
    }
}
