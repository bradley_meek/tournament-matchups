﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TournamentMatchups
{
    internal class Tournament
    {
        private string name { get; set; }

        private List<Player> playerList { get; set; }

        public Tournament(string name)
        {
            this.name = name;
            this.playerList = new List<Player>();
        }

        private void balanceBracket()
        {
            //Determine the number of byes
            int numByes = playerList.Count - ((int)Math.Pow(2, (int)(Math.Log(playerList.Count, 2))));

            for (int i = 1; i <= numByes; i++)
            {
                playerList.Add(new Player(playerList.Count + 1));
                playerList.Last().name = "BYE";
            }

            //Pair off into a new list of players.

            List<Player> bracket = new List<Player>();

            int a = playerList.Count;

            for (int i = 1; i <= (a / 2); i++)
            {
                bracket.Add(playerList.First());
                playerList.Remove(playerList.First());
                bracket.Add(playerList.Last());
                playerList.Remove(playerList.Last());
            }

            //We have our matchups, but to ensure that each round follows best vs worst rule,
            //we need to do some rearranging. Upper and lower brackets should do the trick.
            if (bracket.Count > 4)
            {
                List<Player> upperBracket = new List<Player>();
                List<Player> lowerBracket = new List<Player>();

                for (int i = 1; i <= (a / 2); i++)
                {
                    if (bracket.Count != 0)
                    {
                        upperBracket.Add(bracket.First());
                        bracket.Remove(bracket.First());
                        upperBracket.Add(bracket.First());
                        bracket.Remove(bracket.First());
                    }
                    if (bracket.Count != 0)
                    {
                        lowerBracket.Insert(0, bracket.First());
                        bracket.Remove(bracket.First());
                        lowerBracket.Insert(0, bracket.First());
                        bracket.Remove(bracket.First());
                    }
                }

                upperBracket.AddRange(lowerBracket);

                printBracket(upperBracket);
            }
            else
            {
                printBracket(bracket);
            }
        }

        private void createPlayerList()
        {
            Console.WriteLine("Welcome to {0}'s Bracket Creator", this.name);
            Console.WriteLine("Please enter the player's seed in brackets, followed by their name." +
                "\nExample:\n1\nBradley\nEnter 0 when finished.");

            while (true)
            {
                string input = Console.ReadLine();
                if (input == "0")
                {
                    break;
                }
                else
                {
                    int seed;

                    if (int.TryParse(input, out seed) == true)
                    {
                        playerList.Add(new Player(seed));
                    }
                    else
                    {
                        playerList.Last().name = input;
                    }
                }
            }

            playerList.Sort();

            Console.WriteLine("Thank you.");
        }

        private void printBracket(List<Player> bracket)
        {
            Console.Write("Here are your matchups!");
            int roundNum = 1;
            //print off the bracket here
            for (int i = 0; i < bracket.Count; i += 2)
            {
                Console.Write("\nRound{0}:  {1} ({2})  vs.  {3} ({4})", roundNum, bracket.ElementAt(i).name, bracket.ElementAt(i).seed,
                    bracket.ElementAt(i + 1).name, bracket.ElementAt(i + 1).seed);
                roundNum++;
            }
        }

        public void runTournament()
        {
            createPlayerList();
            balanceBracket();
        }
    }
}