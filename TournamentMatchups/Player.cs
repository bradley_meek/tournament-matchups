﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TournamentMatchups
{
    public class Player : IComparable<Player>
    {
        public String name { get; set; }
        public int seed { get; set; }


        public Player(int seed)
        {
            this.seed = seed;
        }

        public int CompareTo(Player b)
        {
            return this.seed.CompareTo(b.seed);
        }


    }
}
